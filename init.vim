call plug#begin('~/.config/nvim/plugged')

Plug 'myusuf3/numbers.vim'

Plug 'vimwiki/vimwiki'

Plug 'ayu-theme/ayu-vim'

call plug#end()

" Nice terminal colours
set termguicolors

let ayucolor="dark"

colorscheme ayu

" Set colour scheme

" Enable spellchecking
set spelllang=en_gb

autocmd FileType markdown,vimwiki setlocal spell

" Enable line numbers
set number

" Enable inccommand 
set inccommand=split 

" Enable UTF-8
set encoding=utf-8

" Set tab width
set tabstop=4
set shiftwidth=4
set expandtab

" Enable syntax highlighting
syntax on

" Remap semicolon to colon
nnoremap ; :

" Switch between buffers easily
nnoremap <S-n> :bprevious<CR>
nnoremap <S-m> :bnext<CR>

" Easier buffer switching
nnoremap <S-J> <C-W><C-J>
nnoremap <S-K> <C-W><C-K>
nnoremap <S-L> <C-W><C-L>
nnoremap <S-H> <C-W><C-H>

" Exit from terminal mode easily
tnoremap <Esc> <C-\><C-n>   

" Set the leader
let mapleader = "-"

" Toggle a todo with shift-d
nnoremap <S-d> :VimwikiToggleListItem<CR> 

" Open the vimwiki by default
autocmd VimEnter * if argc() == 0 | execute 'VimwikiIndex' | endif

" Push the wiki to the git repo
"autocmd BufWritePost,FileWritePost *.wiki call PushWiki() 

" A function to execute the commands
function PushWiki()
    execute "cd ~/vimwiki"

    execute "make"
endfunction

command Config edit ~/.config/nvim/init.vim
